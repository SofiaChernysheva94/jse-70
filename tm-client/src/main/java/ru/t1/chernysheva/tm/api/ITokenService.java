package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(@Nullable String token);

}
