# TASK MANAGER

## DEVELOPER INFO

* **Name**: Sofia Chernysheva

* **E-mail**: schernysheva@t1-consulting.ru

* **E-mail**: test@test.ru

## SOFTWARE

* **OS**: Windows 10

* **JAVA**: OpenJDK 1.8.0_341

## HARDWARE

* **CPU**: AMD Ryzen 5

* **RAM**: 16GB

* **SSD**: 500GB

## PROGRAM BUILD
```
mvn clean install
```

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```