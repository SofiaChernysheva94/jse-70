package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.TaskDTO;
import ru.t1.chernysheva.tm.model.CustomUser;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskCollectionRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Nullable
    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public List<TaskDTO> get(@AuthenticationPrincipal final CustomUser user) {
        return taskService.findAll(user.getUserId());
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void post(@AuthenticationPrincipal final CustomUser user,
                     @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(user.getUserId(), tasks);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(user.getUserId(), tasks);
    }

    @DeleteMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.removeAll(user.getUserId(), tasks);
    }

}
