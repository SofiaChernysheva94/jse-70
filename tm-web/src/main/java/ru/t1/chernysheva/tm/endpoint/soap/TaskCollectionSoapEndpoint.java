package ru.t1.chernysheva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.soap.*;
import ru.t1.chernysheva.tm.model.CustomUser;

@Endpoint
public class TaskCollectionSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.chernysheva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskDTOService taskDTOService;

    @Nullable
    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "tasksFindAllRequest", namespace = NAMESPACE)
    public TasksFindAllResponse findCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TasksFindAllRequest request) {
        return new TasksFindAllResponse(taskDTOService.findAll(user.getUserId()));
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "tasksSaveRequest", namespace = NAMESPACE)
    public TasksSaveResponse saveCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TasksSaveRequest request) {
        taskDTOService.saveAll(user.getUserId(), request.getTasks());
        return new TasksSaveResponse();
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "tasksUpdateRequest", namespace = NAMESPACE)
    public TasksUpdateResponse updateCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TasksUpdateRequest request) {
        taskDTOService.saveAll(user.getUserId(), request.getTasks());
        return new TasksUpdateResponse();
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "tasksDeleteRequest", namespace = NAMESPACE)
    public TasksDeleteResponse deleteCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final TasksDeleteRequest request) {
        taskDTOService.removeAll(user.getUserId(), request.getTasks());
        return new TasksDeleteResponse();
    }

}
