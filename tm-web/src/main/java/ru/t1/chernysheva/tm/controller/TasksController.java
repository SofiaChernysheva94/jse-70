package ru.t1.chernysheva.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.model.CustomUser;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;


    @GetMapping("/tasks")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
