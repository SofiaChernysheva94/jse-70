package ru.t1.chernysheva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.dto.soap.*;
import ru.t1.chernysheva.tm.model.CustomUser;

@Endpoint
public class ProjectSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.chernysheva.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectDTOService.findOneById(user.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse saveOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectSaveRequest request) {
        projectDTOService.save(user.getUserId(), request.getProject());
        return new ProjectSaveResponse();
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse updateOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectUpdateRequest request) {
        projectDTOService.save(user.getUserId(), request.getProject());
        return new ProjectUpdateResponse();
    }

    @ResponsePayload
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse deleteOne(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectDeleteRequest request) {
        projectService.removeOneById(user.getUserId(), request.getId());
        return new ProjectDeleteResponse();
    }

}
