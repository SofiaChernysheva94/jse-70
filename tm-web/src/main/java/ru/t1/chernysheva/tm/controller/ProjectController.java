package ru.t1.chernysheva.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.CustomUser;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        ProjectDTO project = new ProjectDTO("New Project" + System.currentTimeMillis());
        projectDTOService.save(user.getUserId(), project);
        return "redirect:/projects";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/project/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.removeOneById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("/project/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") ProjectDTO project,
            BindingResult result
    ) {
        projectDTOService.save(user.getUserId(), project);
        return "redirect:/projects";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id) {
        @Nullable final ProjectDTO project = projectDTOService.findOneById(user.getUserId(), id);
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
