package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.TaskDTO;
import ru.t1.chernysheva.tm.model.CustomUser;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Nullable
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public TaskDTO get(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
    }

}
